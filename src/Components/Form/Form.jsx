import React from "react";
import { useForm } from 'react-hook-form'

const Form = () => {
       const {
              register,
              formState: {
                     errors,
                     isValid
              },
              handleSubmit,
              reset
       } = useForm({
              mode: 'all'
       });

       const onSubmit = (data) => {
              alert(JSON.stringify(data))
              reset()
       }
       return (
              <>
                     <h1>React Form Hook</h1>
                     <form onSubmit={handleSubmit(onSubmit)}>
                            <label htmlFor="">
                                   Ferst Name
                                   <input {...register('ferstName', {required: 'Поле обязательно к заполнению', minLength: {value: 4, message: 'Минимум 4 символов'}})} />
                            </label> 
                            <div >{errors?.ferstName && <p style={{ color: 'red', transition: '0.5s all' }}>{errors?.ferstName?.message || "Error"}</p>}</div>
                            {/* Last Name */}
                            <label htmlFor="">
                                   Last Name
                                   <input {...register('lastName', {required: 'Поле обязательно к заполнению', minLength: {value: 6, message: 'Минимум 6 символов'}})} />
                            </label> 
                            <div >{errors?.lastName && <p style={{ color: 'red', transition: '0.5s all'}}>{errors?.lastName?.message || "Error"}</p>}</div>
                            <input style={{ cursor: 'pointer' }}  disabled={!isValid}  type="submit" />
                     </form>
              </>
       )
} 

export default Form